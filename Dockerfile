FROM python:3.6.8

ADD . /app
WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8081

CMD exec gunicorn api_django_callbot_vicidial.wsgi:application --bind 0.0.0.0:8081 --workers 5 --timeout 200

#docker build -t img_voicebot_django:06 .
#docker run -d -p 8081:8081 --name voicebot_django_6 -e TZ=America/Bogota img_voicebot_django:06
