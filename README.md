# MIOS VOICEBOT

## URL

http://127.0.0.1:8000/mios_voicebot/api/

## CGO 
## 1) Insert data

http://127.0.0.1:8000/mios_voicebot/api/cgo/insertdata/

```
{
	"numero":"305717",
	"nombre":"alejandro castellanos",
	"orden":"2045",
	"direccion":"cll 143 a # 20 - 30",
	"franja":"10 am",
	"fecha":"2019-10-30",
	"nombre_carga":"cgo_data_2019-20-30"
	
}
```

## 2) Search number

http://127.0.0.1:8000/mios_voicebot/api/cgo/searchnumber/

```
{
	"numero":"3057131877"
}
```

## 3) Genero marcacion

http://127.0.0.1:8000/mios_voicebot/api/cgo/generomarcacion/

```
{
	"id":"6"
}

```

## 4) Confirma
http://127.0.0.1:8000/mios_voicebot/api/cgo/confirma/

```
{
	"id":"6",
	"confirma":"si"
}
```

## Fin

## Repository
https://alejandrocastellanos_cos@bitbucket.org/alejandrocastellanos_cos/api_django_callbot_asterisk.git