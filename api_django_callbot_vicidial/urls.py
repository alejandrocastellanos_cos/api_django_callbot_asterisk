from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include

urlpatterns = [
    path('mios_voicebot/admin/', admin.site.urls),
    path('mios_voicebot/', include('api_callbot.urls')),
    path('mios_voicebot/login/', include('django.contrib.auth.urls'), {'template_name':'inicio.html'}, name="login")
]