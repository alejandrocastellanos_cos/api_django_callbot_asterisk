# Generated by Django 2.1.13 on 2019-10-21 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_callbot', '0009_montechelo_data_lead_creado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='montechelo_data',
            name='confirma',
            field=models.CharField(default='abandona', max_length=255),
        ),
    ]
