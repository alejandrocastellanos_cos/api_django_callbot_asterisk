# Generated by Django 2.2.6 on 2019-10-08 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_callbot', '0004_montechelo_data_carpeta_especifica'),
    ]

    operations = [
        migrations.AddField(
            model_name='montechelo_data',
            name='cuenta',
            field=models.BigIntegerField(default=0),
        ),
    ]
