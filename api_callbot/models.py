from django.contrib.auth.models import User, Group
from django.db import models
from datetime import datetime
# campaña montechelo
class GroupsCampania(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grupo = models.ForeignKey(Group, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)

class Cgo_data(models.Model):

    #default data
    lead_creado = models.BooleanField(max_length=255, default=False)
    nombre_carga = models.CharField(max_length=255)
    genero_marcacion = models.CharField(max_length=255, default='no')
    confirma = models.CharField(max_length=255, default='abandona')
    fecha_creacion = models.DateTimeField(default=datetime.now)
    grupo = models.CharField(max_length=255, blank=True)
    leadid = models.CharField(max_length=255, blank=True, null=True)
    
    #campos de campania
    numero = models.BigIntegerField()
    nombre = models.CharField(max_length=255)
    orden = models.CharField(max_length=255)
    direccion = models.CharField(max_length=255)
    franja = models.CharField(max_length=255)
    fecha = models.DateField()
    carpeta = models.CharField(max_length=255, default='')
    carpeta_especifica = models.CharField(max_length=255, default='')
    cuenta = models.BigIntegerField(default=0)
    ciudad = models.CharField(max_length=255, default='')
    numero_dos = models.BigIntegerField(default=0)
    prioridad = models.BigIntegerField(default=0)
    
    class Meta:
        ordering = ('id',)
    
    def __str__(self):
        return str(self.numero)

class Laika_data(models.Model): 

    #default data
    lead_creado = models.BooleanField(max_length=255, default=False, blank=True)
    nombre_carga = models.CharField(max_length=255, blank=True)
    genero_marcacion = models.CharField(max_length=255, default='no', blank=True)
    confirma = models.CharField(max_length=255, default='abandona', blank=True)
    fecha_creacion = models.DateTimeField(default=datetime.now)
    grupo = models.CharField(max_length=255, blank=True)
    leadid = models.CharField(max_length=255, blank=True, null=True)

    hora_uno = models.CharField(max_length=255, blank=True)
    hora_dos = models.CharField(max_length=255, blank=True)
    productos = models.CharField(max_length=255, blank=True)
    numero = models.BigIntegerField()
    campania = models.CharField(max_length=255, blank=True)

    class Meta:
        ordering = ('id',)
    
    def __str__(self):
        return str(self.numero)

class Tuya_data(models.Model):

    #campos de campania
    cedula = models.CharField(max_length=255)
    numero = models.BigIntegerField()
    nombre = models.CharField(max_length=255)
    producto = models.CharField(max_length=255)
    saldo = models.BigIntegerField()    
    obligacion = models.BigIntegerField()

    #default data
    nombre_carga = models.CharField(max_length=255)
    lead_creado = models.BooleanField(max_length=255, default=False)
    genero_marcacion = models.CharField(max_length=255, default='no')
    confirma = models.CharField(max_length=255, default='no aplica')
    nombre_audio = models.CharField(max_length=255, default='no aplica')
    leadid = models.CharField(max_length=255, blank=True, null=True)
    fecha_creacion = models.DateTimeField(default=datetime.now)
    grupo = models.CharField(max_length=250, blank=True)
    prioridad = models.BigIntegerField(default=0)

    class Meta:
        ordering = ('id',)
    
    def _str_(self):
        return str(self.numero)