from django.contrib import admin
from .models import Cgo_data, GroupsCampania, Laika_data, Tuya_data

# Register your models here.
admin.site.register(Cgo_data)
admin.site.register(GroupsCampania)
admin.site.register(Laika_data)
admin.site.register(Tuya_data)