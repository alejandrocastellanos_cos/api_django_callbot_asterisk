import csv
import io
import json
import requests
import re
import pandas as pd
from datetime import datetime
from openpyxl import Workbook
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.db import connection
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from . models import Cgo_data, GroupsCampania, Laika_data, Tuya_data
from django.contrib.auth.decorators import login_required, user_passes_test

################################ Review and redirect group ##################################
@login_required(login_url='../../../mios_voicebot/login/login/')
def ValidarUsuario(request):   
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values("grupo_id"))
    
    cursor = connection.cursor()
    nombre_grupo = cursor.execute('''
                                    SELECT name FROM auth_group
                                    WHERE id = '{}'
                                    GROUP BY name
                                    '''.format(get_id_group[0]['grupo_id']))
    res = list(cursor.fetchall())
    otros = ['barranquilla', 'bogota', 'prueba']

    if res[0][0] == 'laika':
        return redirect('../../mios_voicebot/laika/sendcampaign/')
    if res[0][0] in otros:
        return redirect('../../mios_voicebot/cgo/sendcampaign/')
    if res[0][0] == 'tuya':
        return redirect('../../mios_voicebot/tuya/sendcampaign/')

###################################### Campania CGO ###########################################

#API crear registros 
class CGOInsertData(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #variables capturadas por post
            numero = data['numero']
            nombre = data['nombre']
            orden = data['orden']
            direccion = data['direccion']
            franja = data['franja']
            fecha = data['fecha']
            nombre_carga = data['nombre_carga']
            #crear registro en BD
            create_record = Cgo_data.objects.create(numero=numero, nombre=nombre, orden=orden, direccion=direccion, franja=franja, fecha=fecha, nombre_carga=nombre_carga)
            result = {'response':'record created', 'id':create_record.id}

        except Exception as e:
            result = {'response':'error en los datos, verifique.'}
        return Response(result)

#API buscar numero y nombre carga
class CGOSearchNumber(APIView):
    def post(self, request):
        try:
            data = request.data

            #captura datos
            numero = data['numero']
            callerid = data['callerid']
            leadid = callerid[11:]
            leadid_int = int(leadid)
            #buscar numero
            search_number = list(Cgo_data.objects.filter(numero=numero, genero_marcacion='no', leadid=leadid_int).values('id','numero','nombre','orden','direccion','franja','fecha', 'nombre_carga', 'carpeta', 'grupo').order_by('id')[:1:-1])
            result = {'response':'Sin datos para la consulta.'}

            if len(search_number) > 0:
                result = search_number

        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#API actualiza cuando genera marcación
class CGOGeneroMarcacion(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID
            id_number = data['id']
            
            #buscar id
            id_search = list(Cgo_data.objects.filter(pk= id_number).values('genero_marcacion'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Cgo_data.objects.filter(pk= id_number).update(genero_marcacion='si')
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#API actualizar confirmacion
class CGOConfirma(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID  
            id_number = data['id']
            confirma = data['confirma']
            
            #buscar id
            id_search = list(Cgo_data.objects.filter(pk= id_number).values('numero'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Cgo_data.objects.filter(pk= id_number).update(confirma=confirma)
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#marcacion por carga datos
class CGOMarcacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 

            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT SUM(CASE WHEN lead_creado = True THEN 1 ELSE 0 END) as cantidad_leads,  
                                                    SUM(CASE WHEN genero_marcacion = 'si' THEN 1 ELSE 0 END) as marcado
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                '''.format(nombre_carga, prioridad))
           
            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'cantidad_leads': res[x][0],
                        'marcado': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#confirmacion por carga datos
class CGOConfirmacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT confirma as confirmacion, 
                                                count(confirma) as cantidad
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                GROUP BY confirma '''.format(nombre_carga, prioridad))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'confirmacion': res[x][0],
                        'cantidad': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#atiende llamada por carga datos
class CGOAtiendeLlamadaGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT nombre_carga, SUM(CASE 
                                                WHEN confirma = 'si' THEN 1
                                                WHEN confirma = 'no' THEN 1
                                                ELSE 0 END) as contestadas,
                                                SUM(CASE
                                                WHEN confirma = 'abandona' THEN 1 ELSE 0 END) as no_contestadas
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                GROUP BY nombre_carga '''.format(nombre_carga, prioridad))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'nombre_carga': res[x][0],
                        'contestadas': res[x][1],
                        'no_contestadas': res[x][2] 
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#listar prioridades por nombre de cargue
class CGOListarPrioridades(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT DISTINCT prioridad FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}'
                                                ORDER BY prioridad asc '''.format(nombre_carga))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'prioridad': res[x][0], 
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#Vista cargar datos
@login_required(login_url='../../../mios_voicebot/login/login/')
def CGOLoadFile(request):
    template = "loadfile.html"    
    error = ''
    url = 'https://dashbots.outsourcingcos.com/'
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']

    if request.method == "GET":
        lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
        prompt = {
            'url':url,
            'order': 'Orden de los datos para la carga del archivo correctamente: Franja, Fecha(mes/dia/año), Nombre, Dirección, Carpeta, Carpeta especifica, Orden, Cuenta, Ciudad, Telefono 1, Telefono 2, Prioridad',
            'lista_cargas':lista_cargas,
            'grupo':name_group.title()
            }        

        return render(request, template, prompt)

    if request.method == 'POST':
        
        #captura file
        data_file = request.FILES['file']
        #captura nombre carga
        nombre_carga = request.POST['nombre_carga']

        #valida si existe el nombre de la data
        existe_nombre_carga = list(Cgo_data.objects.filter(nombre_carga=nombre_carga).values('id'))
        if len(existe_nombre_carga) > 0:
            messages.error(request, 'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga))
            lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
            response = {
                'url':url,
                'error':'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga),
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
                }
        else:            
            try:
                if data_file.name.endswith('.csv'):
                    #lee el csv
                    data_set = data_file.read().decode('UTF-8')
                    io_string = io.StringIO(data_set)
                    next(io_string)                
                    new_record = False                
                    insert_list = []  

                    #validar delimitador del csv
                    sniffer = csv.Sniffer()
                    dialect = sniffer.sniff(data_set)

                    if dialect.delimiter == ',':          
                        #inserta los registros del csv en la BD dinamicamente
                        for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                            insert_list.append(
                                Cgo_data(
                                    numero = column[9],
                                    nombre = column[2],
                                    orden = column[6],
                                    direccion = column[3],
                                    franja = column[0],
                                    fecha = datetime.strptime(column[1], '%m/%d/%Y'),
                                    nombre_carga = nombre_carga,
                                    carpeta = column[4],
                                    carpeta_especifica = column[5],
                                    cuenta = column[7],
                                    ciudad = column[8],
                                    numero_dos = column[10],
                                    prioridad = column[11],
                                    grupo = name_group
                                )
                            )                 
                            new_record = True
                        Cgo_data.objects.bulk_create(insert_list)
                    
                    if dialect.delimiter == ';':
                        #inserta los registros del csv en la BD dinamicamente
                        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
                            insert_list.append(
                                Cgo_data(
                                    numero = column[9],
                                    nombre = column[2],
                                    orden = column[6],
                                    direccion = column[3],
                                    franja = column[0],
                                    fecha = datetime.strptime(column[1], '%m/%d/%Y'),
                                    nombre_carga = nombre_carga,
                                    carpeta = column[4],
                                    carpeta_especifica = column[5],
                                    cuenta = column[7],
                                    ciudad = column[8],
                                    numero_dos = column[10],
                                    prioridad = column[11],
                                    grupo = name_group
                                )
                            )                 
                            new_record = True
                        Cgo_data.objects.bulk_create(insert_list)   
                
                elif data_file.name.endswith('.xlsx'):
                    
                    df = pd.read_excel(data_file, sheet_name='Hoja1')
                    df_json = json.loads(df.to_json(orient='split'))
                    len_data = len(df_json['data'])
                    num = 0
                    #save_record = False
                    insert_list = []
                    print()

                    for x in range(0, len_data):
                        insert_list.append(
                            Cgo_data(
                                numero = df_json['data'][x][9],
                                nombre = df_json['data'][x][2],
                                orden = df_json['data'][x][6],
                                direccion = df_json['data'][x][3],
                                franja = df_json['data'][x][0],
                                fecha = datetime.strptime(df_json['data'][x][1], '%m/%d/%Y'),
                                nombre_carga = nombre_carga,
                                carpeta = df_json['data'][x][4],
                                carpeta_especifica = df_json['data'][x][5],
                                cuenta = df_json['data'][x][7],
                                ciudad = df_json['data'][x][8],
                                numero_dos = df_json['data'][x][10],
                                prioridad = df_json['data'][x][11],
                                grupo = name_group
                            )
                        )                 
                        new_record = True
                    Cgo_data.objects.bulk_create(insert_list) 

                    #listado de nombre de cargas
                    lista_cargas = list(Cgo_data.objects.values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    new_record = True
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        }
                else:
                    new_record = False
                    messages.error(request, 'Error en el archivo o tipo de archivo no valido. SOLO ARCHIVOS CSV o XLSX')
                    #listado de nombre de cargas
                    lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        'grupo':name_group.title()
                        }
            except Exception as e:                        
                new_record = False
                error = e
            #si creo los registros
            if new_record == True:
                #primeros 5 registros
                data = list(Cgo_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('id','nombre','numero','orden','direccion','franja','fecha','cuenta')[:5])
                #cantidad de registros creados
                quantity_records = str(len(Cgo_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'respuesta':'Datos subidos correctamente',
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'data':data,
                    'grupo':name_group.title()
                    }
            else:                
                #cantidad de registros creados
                quantity_records = str(len(Cgo_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'error_data':'Error en el archivo, verifique si el archivo es correcto. {}'.format(error),
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'grupo':name_group.title()
                    }
        
    
        return render(request, template, response)

#Vista y Enviar datos vicidial
@login_required(login_url='../../../mios_voicebot/login/login/')
def CGOSendCapania(request):
    template = 'sendcampania.html'     
    url_style = 'https://dashbots.outsourcingcos.com/'
    
    #valida el nombre del usuario logueado
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']
    
    if request.method == "GET":  
        try:  
            #capturar campo post
            nombre_carga = request.GET['lista_cargues']
            prioridad = request.GET['prioridad']           

            #datos vicidial
            url = '10.27.0.20'
            usuario = 'cdatos'
            password = 'CD4T0s2019*'
            list_id = '2100'
            
            #consulta bd por nombre carga y lead_creado
            call_data_db = list(Cgo_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga, lead_creado=False, prioridad=prioridad).values('id','numero', 'nombre'))
            lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())  
            existe_carga = list(Cgo_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga, prioridad=prioridad).values('id','numero', 'nombre'))

            result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':'Todos los registros ya fueron enviados',
                    'new_leads':0,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }  

            if len(call_data_db) > 0:
                new_leads = 0
                for x in range(0, len(call_data_db)):
                    telefono = str(call_data_db[x]['numero'])            
                    nombre_cliente = str(call_data_db[x]['nombre'])
                    id_registro = call_data_db[x]['id']

                    get_data= "http://"+url+"/vicidial/non_agent_api.php?source=AgregarLista&user="+usuario+"&pass="+password+"&function=add_lead&phone_number="+telefono+"&phone_code=057&list_id="+list_id+"&add_to_hopper=Y&hopper_priority=99&first_name="+nombre_cliente   
                    result = requests.get(get_data)                 
                   
                    response = result.text
                    print(response)
                    json_vicidial = response.split("|")
                    lead_id = json_vicidial[2]

                    Cgo_data.objects.filter(id=id_registro).update(lead_creado=True, leadid=lead_id)
                    new_leads += 1

                new_lead = 'carga exitosa en vicidial. A iniciado la marcación de la data '+nombre_carga
                
                result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':new_lead,
                    'new_leads':new_leads,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }       

        except Exception as e:
            lista_cargas = list(Cgo_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())  
            result = {
                'url':url_style,
                'sin_datos':'Sin datos para lanzar campaña',
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
            }
        return render(request, template, result)

@login_required(login_url='../../../mios_voicebot/login/login/')
def ExportReportXlsx(request):
    nombre_carga = request.GET['nombre_carga']
    prioridad = request.GET['prioridad'] 

    sms_query = list(Cgo_data.objects.filter(nombre_carga=nombre_carga, prioridad=prioridad).values('numero', 'nombre', 'orden', 'direccion', 'franja', 'fecha', 'nombre_carga', 'genero_marcacion', 'confirma', 'fecha_creacion', 'carpeta', 'carpeta_especifica', 'cuenta', 'ciudad', 'numero_dos', 'lead_creado',))
    
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={}.xlsx'.format(nombre_carga)
    workbook = Workbook()
    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'SMS'
    # Define the titles for columns
    columns = [
        'numero',
        'nombre', 
        'orden', 
        'direccion', 
        'franja', 
        'fecha', 
        'nombre_carga', 
        'genero_marcacion', 
        'confirma', 
        'fecha_creacion', 
        'carpeta', 
        'carpeta_especifica', 
        'cuenta',
        'ciudad',
        'numero_dos',
        'lead_creado',
    ]
    row_num = 1
    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    for x in sms_query:
        row_num += 1
        row = [
            x['numero'],
            x['nombre'],
            x['orden'],
            x['direccion'],
            x['franja'],
            x['fecha'],
            x['nombre_carga'],
            x['genero_marcacion'],
            x['confirma'],
            x['fecha_creacion'],
            x['carpeta'],
            x['carpeta_especifica'],
            x['cuenta'],
            x['ciudad'],
            x['numero_dos'],
            x['lead_creado'],
        ]
        # Assign the data for each cell of the row 
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value
    workbook.save(response)
    return response

def handler404(request):
    return render(request, '404.html', status=404)



###################################### LAIKA ###########################################
@login_required(login_url='../../../mios_voicebot/login/login/')
def LaikaLoadFile(request):
    template = "laikaloadfile.html"    
    error = ''
    url = 'https://dashbots.outsourcingcos.com/'
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']

    if request.method == "GET":
        lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())  
        prompt = {
            'url':url,
            'order': 'Orden de los datos para la carga del archivo correctamente.',
            'lista_cargas':lista_cargas,
            'grupo':name_group.title()
            }        

        return render(request, template, prompt)

    if request.method == 'POST':        
        #captura file
        data_file = request.FILES['file']
        #captura nombre carga
        nombre_carga = request.POST['nombre_carga']
        #campania
        select_campaing = int(request.POST['select_campaing'])

        #valida si existe el nombre de la data
        existe_nombre_carga = list(Laika_data.objects.filter(nombre_carga=nombre_carga).values('id'))
        if len(existe_nombre_carga) > 0:
            messages.error(request, 'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga))
            lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
            response = {
                'url':url,
                'error':'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga),
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
                }
        else:            
            try:
                if data_file.name.endswith('.csv'):
                    #lee el csv
                    data_set = data_file.read().decode('UTF-8')
                    io_string = io.StringIO(data_set)
                    next(io_string)                
                    new_record = False                
                    insert_list = []  

                    #validar delimitador del csv
                    sniffer = csv.Sniffer()
                    dialect = sniffer.sniff(data_set)

                    if dialect.delimiter == ',':          
                        if select_campaing == 1:
                            #inserta los registros del csv en la BD dinamicamente
                            for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                                insert_list.append(
                                    Laika_data(
                                        numero = column[0],
                                        productos = column[1],
                                        hora_uno = column[2],
                                        hora_dos = column[3],
                                        grupo = name_group,
                                        nombre_carga = nombre_carga,
                                        campania = select_campaing
                                    )
                                )                 
                                new_record = True
                            Laika_data.objects.bulk_create(insert_list)
                        if select_campaing > 1:
                            #inserta los registros del csv en la BD dinamicamente
                            for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                                insert_list.append(
                                    Laika_data(
                                        numero = column[0],
                                        grupo = name_group,
                                        nombre_carga = nombre_carga,
                                        campania = select_campaing
                                    )
                                )                 
                                new_record = True
                            Laika_data.objects.bulk_create(insert_list)
                    
                    if dialect.delimiter == ';':
                        if select_campaing == 1:
                            #inserta los registros del csv en la BD dinamicamente
                            for column in csv.reader(io_string, delimiter=';', quotechar="|"):
                                insert_list.append(
                                    Laika_data(
                                        numero = column[0],
                                        productos = column[1],
                                        hora_uno = column[2],
                                        hora_dos = column[3],
                                        grupo = name_group,
                                        nombre_carga = nombre_carga,
                                        campania = select_campaing
                                    )
                                )                 
                                new_record = True
                            Laika_data.objects.bulk_create(insert_list)  
                        
                        if select_campaing > 1:
                            #inserta los registros del csv en la BD dinamicamente
                            for column in csv.reader(io_string, delimiter=';', quotechar="|"):
                                insert_list.append(
                                    Laika_data(
                                        numero = column[0],
                                        grupo = name_group,
                                        nombre_carga = nombre_carga,
                                        campania = select_campaing
                                    )
                                )                 
                                new_record = True
                            Laika_data.objects.bulk_create(insert_list)
                
                elif data_file.name.endswith('.xlsx'):                    
                    df = pd.read_excel(data_file, sheet_name='Hoja1')
                    df_json = json.loads(df.to_json(orient='split'))
                    len_data = len(df_json['data'])
                    num = 0
                    #save_record = False
                    insert_list = []
                    if select_campaing == 1:
                        for x in range(0, len_data):
                            insert_list.append(
                                Laika_data(
                                    numero = df_json['data'][x][0],
                                    productos = df_json['data'][x][1],
                                    hora_uno = df_json['data'][x][2],
                                    hora_dos = df_json['data'][x][3],
                                    grupo = name_group,
                                    nombre_carga = nombre_carga,
                                    campania = select_campaing
                                )
                            )                 
                            new_record = True
                        Laika_data.objects.bulk_create(insert_list) 

                    if select_campaing > 1:
                        for x in range(0, len_data):
                            insert_list.append(
                                Laika_data(
                                    numero = df_json['data'][x][0],
                                    grupo = name_group,
                                    nombre_carga = nombre_carga,
                                    campania = select_campaing
                                )
                            )                 
                            new_record = True
                        Laika_data.objects.bulk_create(insert_list) 

                    #listado de nombre de cargas
                    lista_cargas = list(Laika_data.objects.values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    new_record = True
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        }
                
                else:
                    new_record = False
                    messages.error(request, 'Error en el archivo o tipo de archivo no valido. SOLO ARCHIVOS CSV o XLSX')
                    #listado de nombre de cargas
                    lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        'grupo':name_group.title()
                        }
            except Exception as e:                        
                new_record = False
                error = e
            #si creo los registros
            if new_record == True:
                #primeros 5 registros
                data = list(Laika_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('id','numero')[:5])
                #cantidad de registros creados
                quantity_records = str(len(Laika_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'respuesta':'Datos subidos correctamente',
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'data':data,
                    'grupo':name_group.title()
                    }
            else:                
                #cantidad de registros creados
                quantity_records = str(len(Laika_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'error_data':'{}'.format(error),
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'grupo':name_group.title()
                    }
        
    
        return render(request, template, response)

@login_required(login_url='../../../mios_voicebot/login/login/')
def LaikaSendCampania(request):    
    template = 'laikasendcampania.html'     
    url_style = 'https://dashbots.outsourcingcos.com/'
    
    #valida el nombre del usuario logueado
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']
    
    if request.method == "GET":  
        try:  
            #capturar campo post
            nombre_carga = request.GET['lista_cargues']          
            prioridad = '0'
            #datos vicidial
            url = '10.27.0.10'
            usuario = 'cdatos'
            password = 'CD4T0s2019*'
            list_id = '2100'
            
            #consulta bd por nombre carga y lead_creado
            call_data_db = list(Laika_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga, lead_creado=False).values('id','numero'))
            lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values('nombre_carga').order_by('nombre_carga').distinct())  
            existe_carga = list(Laika_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga).values('id','numero'))

            result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':'Todos los registros ya fueron enviados',
                    'new_leads':0,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }  
            
            if len(call_data_db) > 0:
                new_leads = 0
                for x in range(0, len(call_data_db)):
                    telefono = str(call_data_db[x]['numero'])            
                    nombre_cliente = str('laika lead')
                    id_registro = call_data_db[x]['id']

                    get_data= "http://"+url+"/vicidial/non_agent_api.php?source=AgregarLista&user="+usuario+"&pass="+password+"&function=add_lead&phone_number="+telefono+"&phone_code=057&list_id="+list_id+"&add_to_hopper=Y&hopper_priority=99&first_name="+nombre_cliente   
                    result = requests.get(get_data)                 
                   
                    response = result.text
                    json_vicidial = response.split("|")
                    lead_id = json_vicidial[2]

                    Laika_data.objects.filter(id=id_registro).update(lead_creado=True, leadid=lead_id)
                    new_leads += 1

                new_lead = 'carga exitosa en vicidial. A iniciado la marcación de la data '+nombre_carga
                
                result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':new_lead,
                    'new_leads':new_leads,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }       

        except Exception as e:
            lista_cargas = list(Laika_data.objects.filter(grupo=name_group).values('nombre_carga').order_by('nombre_carga').distinct())
            result = {
                'url':url_style,
                'sin_datos':e,
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
            }
        return render(request, template, result)

#graficas
#grafica marcacion por carga datos
class LaikaMarcacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga'] 

            cursor = connection.cursor()
            estado_marcacion = cursor.execute('''
                                                SELECT SUM(CASE WHEN lead_creado = True THEN 1 ELSE 0 END) as cantidad_leads,  
                                                    SUM(CASE WHEN genero_marcacion = 'si' THEN 1 ELSE 0 END) as marcado
                                                    FROM api_callbot_laika_data
                                                WHERE nombre_carga = '{}'
                                                '''.format(nombre_carga))
           
            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'cantidad_leads': res[x][0],
                        'marcado': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#grafica confirmacion por carga datos
class LaikaConfirmacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            
            cursor = connection.cursor()
            estado_confirmacion = cursor.execute('''
                                                SELECT confirma as confirmacion, 
                                                    count(confirma) as cantidad
                                                    FROM api_callbot_laika_data
                                                WHERE nombre_carga = '{}'
                                                GROUP BY confirma '''.format(nombre_carga))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'confirmacion': res[x][0],
                        'cantidad': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#grafica atiende llamada por carga datos
class LaikaAtiendeLlamadaGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            
            cursor = connection.cursor()
            usuario_atiende = cursor.execute('''
                                                SELECT nombre_carga, SUM(CASE 
                                                    WHEN confirma = 'si' THEN 1
                                                    WHEN confirma = 'no' THEN 1
                                                    ELSE 0 END) as contestadas,
                                                    SUM(CASE
                                                    WHEN confirma = 'abandona' THEN 1 ELSE 0 END) as no_contestadas
                                                    FROM api_callbot_laika_data
                                                WHERE nombre_carga = '{}'
                                                GROUP BY nombre_carga '''.format(nombre_carga))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'nombre_carga': res[x][0],
                        'contestadas': res[x][1],
                        'no_contestadas': res[x][2] 
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

@login_required(login_url='../../../mios_voicebot/login/login/')
def LaikaExportReportXlsx(request):
    nombre_carga = request.GET['nombre_carga']

    sms_query = list(Laika_data.objects.filter(nombre_carga=nombre_carga).values('lead_creado', 'nombre_carga', 'genero_marcacion', 'confirma', 'fecha_creacion', 'grupo', 'leadid', 'hora_uno', 'hora_dos', 'productos', 'numero', 'campania'))
    
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={}.xlsx'.format(nombre_carga)
    workbook = Workbook()
    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'laika'
    # Define the titles for columns
    columns = [
        'lead_creado', 
        'nombre_carga', 
        'genero_marcacion', 
        'confirma', 
        'fecha_creacion', 
        'grupo', 
        'leadid', 
        'hora_uno', 
        'hora_dos', 
        'productos', 
        'numero', 
        'campania',
    ]
    row_num = 1
    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    for x in sms_query:
        row_num += 1
        row = [
            x['lead_creado'],
            x['nombre_carga'],
            x['genero_marcacion'],
            x['confirma'],
            x['fecha_creacion'],
            x['grupo'],
            x['leadid'],
            x['hora_uno'],
            x['hora_dos'],
            x['productos'],
            x['numero'],
            x['campania'],
        ]
        # Assign the data for each cell of the row 
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value
    workbook.save(response)
    return response

#APIs
#Laika API buscar numero y nombre carga
class LaikaCGOSearchNumber(APIView):
    def post(self, request):
        try:
            data = request.data

            #captura datos
            numero = data['numero']
            callerid = data['callerid']
            leadid = callerid[11:]
            leadid_int = int(leadid)
            #buscar numero
            search_number = list(Laika_data.objects.filter(numero=numero, genero_marcacion='no', leadid=leadid_int).values('id', 'lead_creado', 'nombre_carga', 'genero_marcacion', 'confirma', 'fecha_creacion', 'grupo', 'leadid', 'hora_uno', 'hora_dos', 'productos', 'numero', 'campania').order_by('id')[:1:-1])
            result = {'response':'Sin datos para la consulta.'}

            if len(search_number) > 0:
                result = search_number

        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#Laika API actualiza cuando genera marcación
class LaikaCGOGeneroMarcacion(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID
            id_number = data['id']
            
            #buscar id
            id_search = list(Laika_data.objects.filter(pk= id_number).values('genero_marcacion'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Laika_data.objects.filter(pk= id_number).update(genero_marcacion='si')
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#Laika API actualizar confirmacion
class LaikaCGOConfirma(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID  
            id_number = data['id']
            confirma = data['confirma']
            
            #buscar id
            id_search = list(Laika_data.objects.filter(pk= id_number).values('numero'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Laika_data.objects.filter(pk= id_number).update(confirma=confirma)
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)


###################################### Campania TUYA ###########################################
#API crear registros 
class TuyaInsertData(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #variables capturadas por post
            cedula = data['cedula']
            numero = data['numero']
            nombre = data['nombre']
            producto = data['producto']
            saldo = data['saldo']
            obligacion = data['obligacion']
            nombre_carga = data['nombre_carga']
            #crear registro en BD
            create_record = Tuya_data.objects.create(cedula=cedula, numero=numero, nombre=nombre, producto=producto, saldo=saldo, obligacion=obligacion, nombre_carga=nombre_carga)
            result = {'response':'record created', 'id':create_record.id}

        except Exception as e:
            result = {'response':'error en los datos, verifique.'}
        return Response(result)

#API buscar numero y nombre carga
class TuyaSearchNumber(APIView):
    def post(self, request):
        try:
            data = request.data

            #captura datos
            numero = data['numero']
            callerid = data['callerid']
            leadid = callerid[11:]
            leadid_int = int(leadid)
            #buscar numero
            search_number = list(Tuya_data.objects.filter(numero=numero, genero_marcacion='no', leadid=leadid_int).values('id', 'cedula','numero','nombre','producto','saldo','obligacion', 'nombre_carga', 'grupo').order_by('id')[:1:-1])
            result = {'response':'Sin datos para la consulta.'}

            if len(search_number) > 0:
                result = search_number

        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#API actualiza cuando genera marcación
class TuyaGeneroMarcacion(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID
            id_number = data['id']
            
            #buscar id
            id_search = list(Tuya_data.objects.filter(pk= id_number).values('genero_marcacion'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Tuya_data.objects.filter(pk= id_number).update(genero_marcacion='si')
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#API actualizar confirmacion
class TuyaConfirma(APIView):
    def post(self, request):
        try:
            data = request.data
            
            #captura ID  
            id_number = data['id']
            confirma = data['confirma']
            
            #buscar id
            id_search = list(Tuya_data.objects.filter(pk= id_number).values('numero'))
            result = {'response':'Registro no existe'}
            if len(id_search) > 0:
                Tuya_data.objects.filter(pk= id_number).update(confirma=confirma)
                result = {'response':'Registro actualizado'}
        except Exception as e:
            result = {'response':'Error en sus datos, verifique.'}

        return Response(result)

#marcacion por carga datos
class TuyaMarcacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 

            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT SUM(CASE WHEN lead_creado = True THEN 1 ELSE 0 END) as cantidad_leads,  
                                                    SUM(CASE WHEN genero_marcacion = 'si' THEN 1 ELSE 0 END) as marcado
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                '''.format(nombre_carga, prioridad))
           
            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'cantidad_leads': res[x][0],
                        'marcado': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#confirmacion por carga datos
class TuyaConfirmacionGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT confirma as confirmacion, 
                                                count(confirma) as cantidad
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                GROUP BY confirma '''.format(nombre_carga, prioridad))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'confirmacion': res[x][0],
                        'cantidad': res[x][1]
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#atiende llamada por carga datos
class TuyaAtiendeLlamadaGrafica(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            prioridad = request.GET['prioridad'] 
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT nombre_carga, SUM(CASE 
                                                WHEN confirma = 'si' THEN 1
                                                WHEN confirma = 'no' THEN 1
                                                ELSE 0 END) as contestadas,
                                                SUM(CASE
                                                WHEN confirma = 'abandona' THEN 1 ELSE 0 END) as no_contestadas
                                                FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}' and prioridad = '{}'
                                                GROUP BY nombre_carga '''.format(nombre_carga, prioridad))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'nombre_carga': res[x][0],
                        'contestadas': res[x][1],
                        'no_contestadas': res[x][2] 
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#listar prioridades por nombre de cargue
class TuyaListarPrioridades(APIView):
    def get(self, request):
        try:
            nombre_carga = request.GET['nombre_carga']
            
            cursor = connection.cursor()
            estado_mensaje_sms = cursor.execute('''
                                                SELECT DISTINCT prioridad FROM api_callbot_cgo_data
                                                WHERE nombre_carga = '{}'
                                                ORDER BY prioridad asc '''.format(nombre_carga))

            res = list(cursor.fetchall())
            newJson = []
            len_res = len(res)
            if len_res > 0:
                for x in range(0, len_res):
                    newJson += {
                        'prioridad': res[x][0], 
                    },
            return Response(newJson)
        except Exception as e:
            result = {'response':e}
            return Response(result)

#Vista cargar datos
@login_required(login_url='../../../mios_voicebot/login/login/')
def TuyaLoadFile(request):
    template = "tuyaloadfile.html"    
    error = ''
    url = 'https://dashbots.outsourcingcos.com/'
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']

    if request.method == "GET":
        lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
        prompt = {
            'url':url,
            'order': 'Orden de los datos para la carga del archivo correctamente: Cedula, Numero, Nombre, Producto, Saldo, Obligacion',
            'lista_cargas':lista_cargas,
            'grupo':name_group.title()
            }        

        return render(request, template, prompt)

    if request.method == 'POST':
        
        #captura file
        data_file = request.FILES['file']
        #captura nombre carga
        nombre_carga = request.POST['nombre_carga']

        #valida si existe el nombre de la data
        existe_nombre_carga = list(Tuya_data.objects.filter(nombre_carga=nombre_carga).values('id'))
        if len(existe_nombre_carga) > 0:
            messages.error(request, 'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga))
            lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
            response = {
                'url':url,
                'error':'El nombre {} ya existe, por favor ingrese otro nombre.'.format(nombre_carga),
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
                }
        else:            
            try:
                if data_file.name.endswith('.csv'):
                    #lee el csv
                    data_set = data_file.read().decode('UTF-8')
                    io_string = io.StringIO(data_set)
                    next(io_string)                
                    new_record = False                
                    insert_list = []  

                    #validar delimitador del csv
                    sniffer = csv.Sniffer()
                    dialect = sniffer.sniff(data_set)

                    if dialect.delimiter == ',':          
                        #inserta los registros del csv en la BD dinamicamente
                        for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                            insert_list.append(
                                Tuya_data(
                                    cedula = column[0],
                                    numero = column[1],
                                    nombre = column[2],
                                    producto = column[3],
                                    saldo = column[4],
                                    obligacion = column[5],
                                    nombre_carga = nombre_carga,
                                    prioridad = column[15],
                                    grupo = name_group
                                )
                            )                 
                            new_record = True
                        Tuya_data.objects.bulk_create(insert_list)
                    
                    if dialect.delimiter == ';':
                        #inserta los registros del csv en la BD dinamicamente
                        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
                            insert_list.append(
                                Tuya_data(
                                    cedula = column[0],
                                    numero = column[1],
                                    nombre = column[2],
                                    producto = column[3],
                                    saldo = column[4],
                                    obligacion = column[5],
                                    nombre_carga = nombre_carga,
                                    prioridad = column[15],
                                    grupo = name_group                                )
                            )                 
                            new_record = True
                        Tuya_data.objects.bulk_create(insert_list)   
                
                elif data_file.name.endswith('.xlsx'):
                    
                    df = pd.read_excel(data_file, sheet_name='Hoja1')
                    df_json = json.loads(df.to_json(orient='split'))
                    len_data = len(df_json['data'])
                    num = 0
                    #save_record = False
                    insert_list = []
                    print()

                    for x in range(0, len_data):
                        insert_list.append(
                            Tuya_data(
                                cedula = df_json['data'][x][0],
                                numero = df_json['data'][x][1],
                                nombre = df_json['data'][x][2],
                                producto = df_json['data'][x][3],
                                saldo = df_json['data'][x][4],
                                obligacion = df_json['data'][x][5],
                                nombre_carga = nombre_carga,
                                prioridad = df_json['data'][x][15],
                                grupo = name_group
                            )
                        )                 
                        new_record = True
                    Tuya_data.objects.bulk_create(insert_list) 

                    #listado de nombre de cargas
                    lista_cargas = list(Tuya_data.objects.values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    new_record = True
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        }
                else:
                    new_record = False
                    messages.error(request, 'Error en el archivo o tipo de archivo no valido. SOLO ARCHIVOS CSV o XLSX')
                    #listado de nombre de cargas
                    lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                    response = {
                        'url':url,
                        'lista_cargas':lista_cargas,
                        'grupo':name_group.title()
                        }
            except Exception as e:                        
                new_record = False
                error = e
            #si creo los registros
            if new_record == True:
                #primeros 5 registros
                data = list(Tuya_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('id','nombre','numero','orden','direccion','franja','fecha','cuenta')[:5])
                #cantidad de registros creados
                quantity_records = str(len(Tuya_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'respuesta':'Datos subidos correctamente',
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'data':data,
                    'grupo':name_group.title()
                    }
            else:                
                #cantidad de registros creados
                quantity_records = str(len(Tuya_data.objects.filter(nombre_carga = nombre_carga, grupo=name_group).values('numero')))
                #listado de nombre de cargas
                lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())
                #respuesta
                response = {
                    'url':url,
                    'error_data':'Error en el archivo, verifique si el archivo es correcto. {}'.format(error),
                    'cantidad_datos':str('Cantidad de datos guardados = '+quantity_records),
                    'nombre_carga':nombre_carga,
                    'lista_cargas':lista_cargas,
                    'grupo':name_group.title()
                    }
        
    
        return render(request, template, response)

#Vista y Enviar datos vicidial
@login_required(login_url='../../../mios_voicebot/login/login/')
def TuyaSendCapania(request):
    template = 'tuyasendcampania.html'     
    url_style = 'https://dashbots.outsourcingcos.com/'
    
    #valida el nombre del usuario logueado
    user_id = request.user.id
    get_id_group = list(GroupsCampania.objects.filter(user_id=user_id).values('grupo_id'))
    group_id = get_id_group[0]['grupo_id']
    get_name_group = list(Group.objects.filter(id=int(group_id)).values('name'))
    name_group = get_name_group[0]['name']
    
    if request.method == "GET":  
        try:  
            #capturar campo post
            nombre_carga = request.GET['lista_cargues']
            prioridad = request.GET['prioridad']           

            #datos vicidial
            url = '10.27.0.11'
            usuario = 'cdatos'
            password = 'CD4T0s2019*'
            list_id = '2100'
            
            #consulta bd por nombre carga y lead_creado
            call_data_db = list(Tuya_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga, lead_creado=False, prioridad=prioridad).values('id','numero', 'nombre'))
            lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())  
            existe_carga = list(Tuya_data.objects.filter(grupo=name_group, nombre_carga=nombre_carga, prioridad=prioridad).values('id','numero', 'nombre'))

            result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':'Todos los registros ya fueron enviados',
                    'new_leads':0,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }  

            if len(call_data_db) > 0:
                new_leads = 0
                for x in range(0, len(call_data_db)):
                    telefono = str(call_data_db[x]['numero'])            
                    nombre_cliente = str(call_data_db[x]['nombre'])
                    id_registro = call_data_db[x]['id']

                    get_data= "http://"+url+"/vicidial/non_agent_api.php?source=AgregarLista&user="+usuario+"&pass="+password+"&function=add_lead&phone_number="+telefono+"&phone_code=057&list_id="+list_id+"&add_to_hopper=Y&hopper_priority=99&first_name="+nombre_cliente   
                    result = requests.get(get_data)                 
                   
                    response = result.text
                    print(response)
                    json_vicidial = response.split("|")
                    lead_id = json_vicidial[2]

                    Cgo_data.objects.filter(id=id_registro).update(lead_creado=True, leadid=lead_id)
                    new_leads += 1

                new_lead = 'carga exitosa en vicidial. A iniciado la marcación de la data '+nombre_carga
                
                result = {
                    'url':url_style,
                    'nombre_carga':nombre_carga,
                    'prioridad':prioridad,
                    'result':new_lead,
                    'new_leads':new_leads,
                    'lista_cargas':lista_cargas,
                    'existe_carga':len(existe_carga),
                    'grupo':name_group.title()
                }       

        except Exception as e:
            lista_cargas = list(Tuya_data.objects.filter(grupo=name_group).values_list('nombre_carga', flat=True).order_by('nombre_carga').distinct())  
            result = {
                'url':url_style,
                'sin_datos':'Sin datos para lanzar campaña',
                'lista_cargas':lista_cargas,
                'grupo':name_group.title()
            }
        return render(request, template, result)

@login_required(login_url='../../../mios_voicebot/login/login/')
def TuyaExportReportXlsx(request):
    nombre_carga = request.GET['nombre_carga']
    prioridad = request.GET['prioridad'] 

    sms_query = list(Tuya_data.objects.filter(nombre_carga=nombre_carga, prioridad=prioridad).values('cedula', 'numero', 'nombre', 'producto', 'saldo', 'obligacion', 'nombre_carga', 'lead_creado', 'genero_marcacion', 'confirma', 'nombre_audio', 'leadid', 'fecha_creacion', 'grupo', 'prioridad',))
    
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={}.xlsx'.format(nombre_carga)
    workbook = Workbook()
    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'SMS'
    # Define the titles for columns
    columns = [
        'cedula',
        'numero',
        'nombre', 
        'producto', 
        'saldo', 
        'obligacion', 
        'nombre_carga',
        'lead_creado', 
        'genero_marcacion', 
        'confirma', 
        'nombre_audio',
        'leadid',
        'fecha_creacion', 
        'grupo', 
        'prioridad',
    ]
    row_num = 1
    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    for x in sms_query:
        row_num += 1
        row = [
            x['cedula'],
            x['numero'],
            x['nombre'],
            x['producto'],
            x['saldo'],
            x['obligacion'],
            x['nombre_carga'],
            x['lead_creado'],
            x['genero_marcacion'],
            x['confirma'],
            x['nombre_audio'],
            x['leadid'],
            x['fecha_creacion'],
            x['grupo'],
            x['prioridad'],
        ]
        # Assign the data for each cell of the row 
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value
    workbook.save(response)
    return response

def handler404(request):
    return render(request, '404.html', status=404)
