from django.apps import AppConfig


class ApiCallbotConfig(AppConfig):
    name = 'api_callbot'
