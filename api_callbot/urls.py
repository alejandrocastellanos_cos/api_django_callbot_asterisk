from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CGOInsertData, CGOSearchNumber, CGOGeneroMarcacion, CGOConfirma, CGOLoadFile, CGOSendCapania, CGOMarcacionGrafica, CGOConfirmacionGrafica, CGOAtiendeLlamadaGrafica, ExportReportXlsx, CGOListarPrioridades, handler404, LaikaSendCampania, ValidarUsuario, LaikaLoadFile, LaikaMarcacionGrafica, LaikaConfirmacionGrafica, LaikaAtiendeLlamadaGrafica, LaikaExportReportXlsx, LaikaCGOSearchNumber, LaikaCGOGeneroMarcacion, LaikaCGOConfirma, TuyaInsertData, TuyaSearchNumber, TuyaGeneroMarcacion, TuyaConfirma, TuyaMarcacionGrafica, TuyaConfirmacionGrafica, TuyaAtiendeLlamadaGrafica, TuyaListarPrioridades, TuyaLoadFile, TuyaSendCapania, TuyaExportReportXlsx

urlpatterns = [
    #CGO
    path("cgo/api/insertdata/", CGOInsertData.as_view()),
    path("cgo/api/searchnumber/", CGOSearchNumber.as_view()),
    path("cgo/api/generomarcacion/", CGOGeneroMarcacion.as_view()),
    path("cgo/api/confirma/", CGOConfirma.as_view()),    
    path("cgo/api/grafica_marcacion/", CGOMarcacionGrafica.as_view()),
    path("cgo/api/grafica_confirmacion/", CGOConfirmacionGrafica.as_view()),
    path("cgo/api/grafica_atiendellamda/", CGOAtiendeLlamadaGrafica.as_view()),
    path("cgo/api/listaprioridad/", CGOListarPrioridades.as_view()),
    path("review_group/", ValidarUsuario, name="reviewgroup"),
    path("cgo/loadfile/", CGOLoadFile, name="loadfile"),
    path("cgo/sendcampaign/", CGOSendCapania, name="sendcapaign"),
    path("cgo/export/", ExportReportXlsx, name="export_xlsx"),
    #LAIKA
    path("laika/sendcampaign/", LaikaSendCampania, name="laika_sendcampaign"),
    path("laika/loadfile/", LaikaLoadFile, name="laika_loadfile"),
    path("laika/export/", LaikaExportReportXlsx, name="laika_export_xlsx"),
    path("laika/api/grafica_marcacion/", LaikaMarcacionGrafica.as_view()),
    path("laika/api/grafica_confirmacion/", LaikaConfirmacionGrafica.as_view()),
    path("laika/api/grafica_atiendellamda/", LaikaAtiendeLlamadaGrafica.as_view()),
    path("laika/api/searchnumber/", LaikaCGOSearchNumber.as_view()),
    path("laika/api/generomarcacion/", LaikaCGOGeneroMarcacion.as_view()),
    path("laika/api/confirma/", LaikaCGOConfirma.as_view()), 
    #TUYA
    path("tuya/api/insertdata/", TuyaInsertData.as_view()),
    path("tuya/api/searchnumber/", TuyaSearchNumber.as_view()),
    path("tuya/api/generomarcacion/", TuyaGeneroMarcacion.as_view()),
    path("tuya/api/confirma/", TuyaConfirma.as_view()),    
    path("tuya/api/grafica_marcacion/", TuyaMarcacionGrafica.as_view()),
    path("tuya/api/grafica_confirmacion/", TuyaConfirmacionGrafica.as_view()),
    path("tuya/api/grafica_atiendellamda/", TuyaAtiendeLlamadaGrafica.as_view()),
    path("tuya/api/listaprioridad/", TuyaListarPrioridades.as_view()),
    path("review_group/", ValidarUsuario, name="reviewgroup"),
    path("tuya/loadfile/", TuyaLoadFile, name="loadfile"),
    path("tuya/sendcampaign/", TuyaSendCapania, name="sendcapaign"),
    path("tuya/export/", TuyaExportReportXlsx, name="export_xlsx"),

]
urlpatterns = format_suffix_patterns(urlpatterns)
handler404 = handler404